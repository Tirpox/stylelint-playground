# Stylelint playground

This repos is for testing out [stylelint](https://stylelint.io/) with various
different task managers

## Installation

```
npm install
```

## Vanilla

```
> npm run stylelint
```

## Grunt

```
> grunt
```

## Gulp

```
> gulp
```

## Webpack

Coming soon
