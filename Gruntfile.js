module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    stylelint: {
      all: ['styles/css/**/*.css', 'styles/scss/**/*.scss']
    }
  });

  // Load the plugin that provides the "stylelint" task.
  grunt.loadNpmTasks('grunt-stylelint');

  // Default task(s).
  grunt.registerTask('default', ['stylelint']);
};
